package umfds.poste_finale;

public class ColisExpressInvalide extends Exception {

	public ColisExpressInvalide(String string) {
		super(string);
	}

}
