package umfds.poste_finale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;


class ColisTest {
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	Colis colis1;
	
	@BeforeEach
	void init() {
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	}
	
	@Test
	void testToStringCorrects() {
		assertAll("toStringOk", 
				()-> assertThat(colis1, hasToString("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0")));
	}
	
	@Test
	void testAffranchissements() {
		assertAll("AffranchissementOk",
				()-> assertThat(Math.abs(colis1.tarifAffranchissement()-3.5f), lessThan(tolerancePrix)));
	}
	
	@Test
	void testRemboursement() {
		assertAll("RemboursementOk",
				()-> assertThat(Math.abs(colis1.tarifRemboursement()-100.0f), lessThan(tolerancePrix)));
	}

	

}
