package umfds.poste_finale;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class ColisExpressTest {

	@Test
	void testLeveeExceptionCreationColisExpressPlus30Kilos() {
		assertThrows(ColisExpressInvalide.class,
				()->{
					new ColisExpress("Le pere Noel", 
							"famille Kaya, igloo 10, terres ouest",
							"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200, true);
				});
	}
	
	@Test
	void testLeveeExceptionCreationColisExpress30Kilos() {
		assertThrows(ColisExpressInvalide.class,
				()->{
					new ColisExpress("Le pere Noel", 
							"famille Kaya, igloo 10, terres ouest",
							"7877", 30, 0.02f, Recommandation.deux, "train electrique", 200, true);
				});
	}
	
	@Test
	@Disabled
	void testLeveeExceptionCreationColisExpressMoins30Kilos() {
		assertThrows(ColisExpressInvalide.class,
				()->{
					new ColisExpress("Le pere Noel", 	
							"famille Kaya, igloo 10, terres ouest",
							"7877", 15, 0.02f, Recommandation.deux, "train electrique", 200, true);
				});
	}


}
