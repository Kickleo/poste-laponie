package umfds.poste_finale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class SacPostalTest {

	private static double tolerancePrix=0.001f;
	private static double toleranceVolume=0.0000001f;
	Lettre lettre1, lettre2;
	Colis colis1;
	SacPostal sac1, sac2;
	
	@BeforeEach
	 void setUp() {
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
		sac2 = sac1.extraireV1("7877");
	}
	
	@Test
	@Disabled
	void testValeurRemboursement() {
		assertThat(Math.abs(sac1.valeurRemboursement()-116.5f), lessThan(tolerancePrix));
	}
	
	@Test
	@Disabled
	void testGetVolume() {
		assertAll("Volume Ok",
				()-> assertThat(Math.abs(sac1.getVolume()-0.025359999558422715f), lessThan(toleranceVolume)),
				()-> assertThat(Math.abs(sac2.getVolume()-0.02517999955569394f), lessThan(toleranceVolume)));
		
	}
	


}
