package umfds.poste_finale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LettreTest {
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	Lettre lettre1, lettre2;
	
	
	@BeforeEach
	void init() {
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
	}
	
	@Test
	void testToStringCorrects() {
		assertAll("toStringOk", 
				()-> assertThat(lettre1, hasToString("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire")),	
				()-> assertThat(lettre2, hasToString("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence")));
	}
	
	@Test
	void testAffranchissements() {
		assertAll("AffranchissementOk", 
				()-> assertThat(Math.abs(lettre1.tarifAffranchissement()-1.0f), lessThan(tolerancePrix)), 	
				()-> assertThat(Math.abs(lettre2.tarifAffranchissement()-2.3f), lessThan(tolerancePrix)));
	}
	
	@Test
	void testRemboursement() {
		assertAll("RemboursementOk", 
				()-> assertThat(Math.abs(lettre1.tarifRemboursement()-1.5f), lessThan(tolerancePrix)), 	
				()-> assertThat(Math.abs(lettre2.tarifRemboursement()-15.0f), lessThan(tolerancePrix)));
	}

	

}
